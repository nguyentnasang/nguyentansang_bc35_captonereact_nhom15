import { https } from './configURL';

export const getMovie = () => {
  return https.get('/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07');
};
export const getMovieItem = (maPhim) => {
  return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
};
export const getMovieTheater = () => {
  return https.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap`);
};
export const postMovie = (data) => {
  return https.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, data);
};
export const postSeat = (data) => {
  return https.post(`api/QuanLyDatVe/DatVe`, data);
};

export const getMovieSchedule = (maPhim) => {
  return https.get(`api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`);
};

export const getMovieTicketStatus = (maPhim) => {
  return https.get(`api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maPhim}`);
};
