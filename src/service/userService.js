import { https } from "./configURL";

export const postLogin = (user) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", user);
};
export const postRegis = (user) => {
  return https.post("/api/QuanLyNguoiDung/DangKy", user);
};
export const getUserList = () => {
  return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00");
};
export const deleteUser = (account) => {
  return https.delete(
    `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${account}`
  );
};
export const postUser = (taiKhoan) => {
  return https.post(
    `/api/QuanLyNguoiDung/LayThongTinNguoiDung?taiKhoan=${taiKhoan}`
  );
};
export const posttUser = (data) => {
  return https.post(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, data);
};
export const posttNewUser = (data) => {
  return https.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, data);
};
export const getFindUser = (data) => {
  return https.get(
    `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=${data.MaNhom}&tuKhoa=${data.tuKhoa}`
  );
};
