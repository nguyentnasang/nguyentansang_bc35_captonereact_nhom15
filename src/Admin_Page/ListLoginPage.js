import React, { useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import { deleteUser, getUserList, postUser } from "./../service/userService";
import FormUser from "./FormUser";
import { userLocalService } from "../service/localService";
import { useDispatch, useSelector } from "react-redux";
import { setUserArrInfo } from "../redux-toolkit/userSlice";
import FindUser from "./FindUser";
import { useNavigate } from "react-router-dom";
import { setLoadingOff } from "../redux-toolkit/spinnerSlice";
export default function ListLoginPage() {
  let navigate = useNavigate();
  let findUsers = useSelector((state) => {
    return state.userSlice.findUser;
  });
  let [dataArr, setDataArr] = useState([]);
  const renderListUserArr = () => {
    if (findUsers) {
      return findUsers;
    } else {
      return dataArr;
    }
  };
  let dispatch = useDispatch();
  // console.log("user", user);
  useEffect(() => {
    let postUserItem = (taiKhoan) => {
      postUser(taiKhoan)
        .then((res) => {
          dispatch(setUserArrInfo(res.data.content));
          // console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    const handleDeleteUser = (item) => {
      deleteUser(item)
        .then((res) => {
          console.log(res);
          message.success("xóa thành công");
          fetchUserList();
        })
        .catch((err) => {
          console.log(err);
          message.error("xóa thất bại");
          dispatch(setLoadingOff(false));
        });
    };
    let fetchUserList = () => {
      getUserList()
        .then((res) => {
          // console.log(res);
          let userList = res.data.content.map((item) => {
            return {
              ...item,
              action: (
                <>
                  <button
                    onClick={() => {
                      handleDeleteUser(item.taiKhoan);
                    }}
                    className="bg-red-500 px-2 py-1 rounded"
                  >
                    xóa
                  </button>
                  <button
                    onClick={() => {
                      postUserItem(item.taiKhoan);
                    }}
                    className="bg-blue-500 px-2 py-1 rounded ml-2"
                  >
                    sửa
                  </button>
                </>
              ),
            };
          });
          setDataArr(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchUserList();
  }, []);

  const columns = [
    {
      title: "Tài Khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Họ Tên",
      key: "hoTen",
      dataIndex: "hoTen",
    },
    {
      title: "Số Điện Thoại",
      dataIndex: "soDT",
      key: "soDT",
    },
    {
      title: "Email",
      key: "email",
      dataIndex: "email",
    },
    {
      title: "Mật Khẩu",
      dataIndex: "matKhau",
      key: "matKhau",
    },
    {
      title: "Mã Loại Người Dùng",
      key: "maLoaiNguoiDung",
      dataIndex: "maLoaiNguoiDung",
      render: (text) => {
        if (text == "QuanTri") {
          return <Tag color="red">Admin</Tag>;
        }
        {
          return <Tag color="blue">User</Tag>;
        }
      },
    },
    {
      title: "Hành Động",
      dataIndex: "action",
      key: "action",
    },
  ];

  return (
    <div>
      <h4 className="my-5 font-medium text-4xl">QUẢN LÝ NGƯỜI DÙNG</h4>
      <div className="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2">
        <FormUser />
        <FindUser />
      </div>
      <Table columns={columns} dataSource={renderListUserArr()} />;
    </div>
  );
}
