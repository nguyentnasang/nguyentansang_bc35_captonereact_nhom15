import React, { useState } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { deleteUser, getFindUser } from "../service/userService";
import { useDispatch, useSelector } from "react-redux";
import { setUserArrInfo, setUserFindInFo } from "../redux-toolkit/userSlice";
import { postUser } from "./../service/userService";
import { userLocalStoreChange } from "../service/localService";
export default function FindUser() {
  let dispatchUserArr = useDispatch();

  const handlePostUser = (taiKhoan) => {
    postUser(taiKhoan)
      .then((res) => {
        console.log(res);

        dispatchUserArr(setUserArrInfo(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const deleteUserItem = (item) => {
    deleteUser(item)
      .then((res) => {
        console.log(res);
        message.success("xóa thành công");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("xóa thất bại");
      });
  };
  let [userFind, setUserFind] = useState([]);
  let dispatch = useDispatch();
  const onFinish = (values) => {
    let find = { ...values, MaNhom: "GP00" };
    getFindUser(find)
      .then((res) => {
        // console.log(res);
        setUserFind(res.data.content);
        console.log("res.data.content", res.data.content);
        let dataUserFind = () => {
          return res.data.content.map((item) => {
            return {
              ...item,
              action: (
                <>
                  <button
                    onClick={() => {
                      deleteUserItem(item.taiKhoan);
                    }}
                    className="bg-red-500 px-2 py-1 rounded"
                  >
                    xóa
                  </button>
                  <button
                    onClick={() => {
                      handlePostUser(item.taiKhoan);
                    }}
                    className="bg-blue-500 px-2 py-1 rounded ml-2"
                  >
                    sửa
                  </button>
                </>
              ),
            };
          });
        };
        // console.log("dataUserFind", dataUserFind);
        dispatch(setUserFindInFo(dataUserFind()));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      {" "}
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Từ Khóa"
          name="tuKhoa"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        ></Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className="bg-slate-500" htmlType="submit">
            Tìm Kiếm
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
