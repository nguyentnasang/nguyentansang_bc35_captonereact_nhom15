import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postLogin } from "./../service/userService";
import { userLocalService } from "../service/localService";
export default function AdminLogin() {
  const onFinish = (values) => {
    console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        console.log(res);
        userLocalService.set(res.data.content);
        if (res.data.content.maLoaiNguoiDung != "QuanTri") {
          message.error("Bạn không phải là USER");
          setTimeout(() => {
            window.location.href = "/";
          }, 1500);
        } else {
          message.success("Đăng nhập thành công");
          setTimeout(() => {
            window.location.href = "/admin/listlogin";
          }, 1000);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="text-left bg-gradient-to-r from-orange-300 to-yellow-200 h-screen px-60">
      <p className="flex justify-center items-center text-3xl pt-5">
        ĐĂNG NHẬP ADMIN
      </p>
      <Form
        layout="vertical"
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 24,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 24,
          }}
        ></Form.Item>

        <Form.Item
          className="flex justify-center items-center w-full"
          wrapperCol={{
            offset: 8,
            span: 24,
          }}
        >
          <div className="mb-5 w-full">
            đăng nhập user_
            <button
              className="font-medium animate-pulse"
              onClick={() => {
                window.location.href = "/login";
              }}
            >
              tại đây
            </button>
          </div>
          <Button className="bg-green-500 mr-4" htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
