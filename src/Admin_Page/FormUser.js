import React, { useState } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { useSelector } from "react-redux";
import { userLocalStoreChange } from "../service/localService";
import { posttNewUser, posttUser, putUser } from "../service/userService";
export default function FormUser() {
  let [newUserArr, setNewUserArr] = useState([]);
  let userChange = useSelector((state) => {
    return state.userSlice.userArr;
  });
  userLocalStoreChange.set(userChange);
  let user = userLocalStoreChange.get();
  console.log("user", user);
  let pennant = true;
  const onFinish = (values) => {
    console.log("Success:", values);
    if (pennant) {
      //thêm mới

      posttNewUser(values)
        .then((res) => {
          console.log(res);
          message.success("thêm thành công");
        })
        .catch((err) => {
          console.log(err);
          message.error("thêm thất bại");
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        });
    } else {
      //sửa
      setNewUserArr(values);
      posttUser(values)
        .then((res) => {
          console.log(res);
          message.success("sửa thành công");
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        })
        .catch((err) => {
          console.log(err);
          message.error("sửa thất bại");
        });
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <Form
        fields={[
          {
            name: ["taiKhoan"],
            value: user.taiKhoan,
          },
          {
            name: ["matKhau"],
            value: user.matKhau,
          },
          {
            name: ["email"],
            value: user.email,
          },
          {
            name: ["soDT"],
            value: user.soDT,
          },
          {
            name: ["maNhom"],
            value: user.maNhom,
          },
          {
            name: ["maLoaiNguoiDung"],
            value: user.maLoaiNguoiDung,
          },
          {
            name: ["hoTen"],
            value: user.hoTen,
          },
        ]}
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
          ["name"]: user.taiKhoan,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Tài Khoản"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>

        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>
        <Form.Item
          label="Số Điện Thoại"
          name="soDT"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>
        <Form.Item
          label="Mã Nhóm"
          name="maNhom"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>
        <Form.Item
          label="Mã Loại Người Dùng"
          name="maLoaiNguoiDung"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>
        <Form.Item
          label="Họ Tên"
          name="hoTen"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input defaultValue={user.taiKhoan} />
        </Form.Item>
        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        ></Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            onClick={() => {
              pennant = false;
            }}
            className="bg-blue-500"
            htmlType="submit"
          >
            Cập Nhật
          </Button>
          <Button
            onClick={() => {
              pennant = true;
            }}
            className="bg-green-600"
            htmlType="submit"
          >
            Thêm Mới
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
