import React from "react";
import Background from "../../img/backgroud.jpg";

export default function Apppage() {
  return (
    <div
      className="w-screen flex justify-center items-center p-5 text-white lg:p-20 md:p-10"
      style={{ backgroundImage: "url(" + Background + ")" }}
    >
      <div>
        <h2 className="text-4xl">
          Ứng dụng tiện lợi dành cho người yêu điện ảnh
        </h2>
        <p className="my-5">
          Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi
          quà hấp dẫn.
        </p>
        <button className="text-black bg-yellow-300 text-2xl py-2 px-5 rounded hover:bg-yellow-500 hover:text-gray-700">
          APP MIỄN PHÍ - TẢI VỀ NGAY
        </button>
        <p className="mt-5">TIX có hai phiên bản IOS & Android</p>
      </div>
      <img
        className="rounded ml-5 hidden lg:block md:block"
        width={300}
        src={require("../../img/mobile.jpg")}
        alt=""
      />
    </div>
  );
}
