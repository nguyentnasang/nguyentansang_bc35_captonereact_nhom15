import React from 'react';
import { AiOutlineDelete } from 'react-icons/ai';
import { useDispatch } from 'react-redux';
import { setSelectedList } from '../../redux-toolkit/seatSlice';
export default function SelectedSeats({ danhSachDangChon, tenPhim }) {
  const dispatch = useDispatch();
  return (
    <>
      {danhSachDangChon.map((item) => {
        return (
          <div className="w-full text-left bg-gradient-to-tr from-[#353535] to-[#504e4e]  rounded-xl flex justify-between items-center px-5 py-5 space-x-4">
            <div className="lg:w-[40px] w-[25px] text-sm lg:text-base flex justify-center py-2 rounded-md bg-color3 text-white max-w-full">
              {item.tenGhe}
            </div>
            <div>
              <h3 className="text-gray-400 text-md">{tenPhim}</h3>
              <p className="text-left text-gray-100 font-semibold">
                {item.giaVe}
              </p>
            </div>
            <AiOutlineDelete
              onClick={() => {
                dispatch(setSelectedList(item));
              }}
              className="text-red-500 cursor-pointer"
            />
          </div>
        );
      })}
    </>
  );
}
