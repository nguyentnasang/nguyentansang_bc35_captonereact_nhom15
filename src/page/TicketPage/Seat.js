import React from 'react';
import { useState } from 'react';
import { setSelectedList } from '../../redux-toolkit/seatSlice';
import { useDispatch } from 'react-redux';

export default function Seat({ seat, movieInfo }) {
  const dispatch = useDispatch();
  const [dangChon, setDangChon] = useState(false);
  const handleDangChon = () => {
    dangChon === false ? setDangChon(true) : setDangChon(false);
    dispatch(setSelectedList(seat));
  };
  return (
    <>
      <button
        onClick={handleDangChon}
        className={`lg:w-[40px] w-[25px] py-1 lg:py-2 flex justify-center text-sm lg:text-base border rounded ${
          seat?.daDat == true
            ? '!bg-color2 !pointer-events-none cursor-not-allowed'
            : ''
        } ${seat.loaiGhe == 'Vip' ? 'bg-color4' : ''} ${
          dangChon == true ? '!bg-color3' : ''
        }`}
      >
        {seat.tenGhe}
      </button>
    </>
  );
}
