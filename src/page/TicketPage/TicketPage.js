import React, { useState } from 'react';
import { useEffect } from 'react';
import { getMovieTicketStatus } from '../../service/movieService';
import { useParams } from 'react-router-dom';
import { seats } from './seatConstants';
import Seat from './Seat';
import { useSelector } from 'react-redux';
import { postSeat } from './../../service/movieService';
import { message } from 'antd';
import moment from 'moment';
import 'moment/locale/vi';
import SelectedSeats from './SelectedSeats';

export default function TicketPage() {
  const danhSachDangChon = useSelector((state) => state.seatSlice.selectedList);
  // console.log(danhSachDangChon);
  const danhSachVe = useSelector((state) => state.seatSlice.dangChonList);

  const [danhSachGhe, setDanhSachGhe] = useState([]);
  const [movieInfo, setMovieInfo] = useState({});

  const { maLichChieu } = useParams();
  console.log(maLichChieu);
  useEffect(() => {
    getMovieTicketStatus(maLichChieu)
      .then((res) => {
        // console.log(res.data.content);
        setDanhSachGhe(res.data.content.danhSachGhe);
        setMovieInfo(res.data.content.thongTinPhim);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const handleDatVe = () => {
    postSeat({
      maLichChieu: maLichChieu,
      danhSachVe: danhSachVe,
    })
      .then((res) => {
        console.log(res);
        message.success('Đặt vé thành công ');
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((err) => {
        message.error('Vui lòng thử lại');
        console.log(err);
      });
  };
  return (
    <div className="font-libre tracking-wide py-36 bg-gradient-to-tr from-color5 to-[#191b22] flex items-center justify-center">
      <div className="flex min-h-full lg:flex-row flex-col space-x-4 px-5">
        <div className="lg:w-3/4 w-full flex h-full flex-col justify-center items-center rounded-2xl bg-gradient-to-tr from-color5 to-[#504e4e] md:px-24 px-10 py-10  ">
          <div className="flex items-center justify-center rounded-xl w-full h-20 bg-color4 text-md text-gray-700">
            Screen
          </div>
          <div className="flex h-full space-x-3 text-white my-10">
            <div className="flex flex-col gap-y-5">
              {seats.map((seat) => {
                return <p>{seat}</p>;
              })}
            </div>
            <div className="grid grid-cols-16 gap-x-5 gap-y-5 ">
              {danhSachGhe.map((ghe) => (
                <Seat movieInfo={movieInfo} seat={ghe} />
              ))}
            </div>
          </div>
          <div className="flex space-x-3 w-full justify-center border rounded-xl py-4 mt-5">
            <div className="flex items-center space-x-2">
              <div className="bg-color3 lg:p-5 p-2 border rounded "></div>
              <span className="text-gray-200 md:text-base text-sm ">
                Đang chọn
              </span>
            </div>
            <div className="flex space-x-2 items-center">
              <div className="bg-color4 lg:p-5 p-2  border rounded "></div>
              <span className="text-gray-200 md:text-base text-sm ">
                Ghế VIP
              </span>
            </div>
            <div className="flex space-x-2 items-center">
              <div className="lg:p-5 p-2 border rounded "></div>
              <span className="text-gray-200 md:text-base text-sm ">
                Chưa đặt
              </span>
            </div>
            <div className="flex space-x-2 items-center">
              <div className="lg:p-5 p-2 bg-color2  border rounded "></div>
              <span className="text-gray-200 md:text-base text-sm">Đã đặt</span>
            </div>
          </div>
        </div>
        <div className="lg:w-1/4 min-h-full w-full flex flex-col justify-between rounded-2xl bg-gradient-to-tr from-[#1b1b1b] to-[#2c2c2c]  px-7 py-10 ">
          <div className="flex items-center">
            <div className="h-36 flex items-center max-w-[150px] overflow-hidden">
              <img
                className="w-full object-center rounded-2xl"
                src={movieInfo?.hinhAnh}
                alt=""
              />
            </div>
            <div className="lg:pl-2 pl-5 flex flex-col justify-between ">
              <div className="truncate">
                <p className="text-white text-md font-semibold">
                  {movieInfo.tenPhim}
                </p>
                <p className="text-gray-400 text-sm ">
                  {movieInfo.gioChieu} {moment(movieInfo.ngayChieu).format('L')}
                </p>
              </div>
              <p className="text-gray-100 text-sm mt-5">
                {movieInfo.tenCumRap}
              </p>
            </div>
          </div>
          <div className="grow">
            <h3 className="text-gray-100 text-md text-2xl font-bold text-left border-b border-gray-400 h-20 flex items-center">
              Đang Chọn
            </h3>
            <div className="my-3 flex flex-col space-y-2">
              <SelectedSeats
                tenPhim={movieInfo.tenPhim}
                danhSachDangChon={danhSachDangChon}
              />
            </div>
          </div>
          <div className="border-t h-20 flex flex-col items-center border-gray-400 gap-y-3">
            <p className="text-gray-200 w-full text-left font-semibold text-xl my-3">
              {`Tổng cộng: ${danhSachDangChon.reduce(
                (partialSum, item) => partialSum + item.giaVe,
                0
              )}  VND`}
            </p>
            <button
              className="bg-color2 text-xl hover:brightness-75 duration-150 text-white font-semibold inline-block w-full py-2 rounded-xl"
              onClick={handleDatVe}
            >
              Đặt Vé
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
