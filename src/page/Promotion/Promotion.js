import React from "react";

export default function Promotion() {
  return (
    <div className="text-2xl pt-24">
      {/* ............  */}
      <div className="my-10 flex mx-5">
        <img width={500} src={require("../../img/bkt.jpg")} alt="" />
        <div className=" ml-5 text-left hidden lg:block md:block">
          <b className="truncate mt-2">
            [123Phim] NHẬP MÃ 'BKT' - Giảm ngay 20k khi đặt vé Bắc Kim Thang
          </b>
          <p className="mt-10">
            123Phim đồng hành cùng phim Việt - Giảm ngay 20k mỗi giao dịch khi
            đặt vé Bắc Kim Thang trên ứng dụng 123Phim.
          </p>
        </div>
      </div>

      {/* ................  */}
      <div className="my-10 flex mx-5">
        <img width={500} src={require("../../img/snmg.jpg")} alt="" />
        <div className=" ml-5 text-left hidden lg:block md:block">
          <b className="truncate mt-2">Sinh Nhật Mega GS</b>
          <p className="mt-10">
            Đến hẹn lại lên, vậy là một năm nữa đã trôi qua và chúng ta lại đến
            tháng 8, tháng sinh nhật của Mega GS Cinemas.
          </p>
        </div>
      </div>
    </div>
  );
}
