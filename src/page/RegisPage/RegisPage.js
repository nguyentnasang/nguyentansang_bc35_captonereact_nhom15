import React from "react";
import { Button, Form, Input, message, Select, TreeSelect } from "antd";
import { Option } from "antd/es/mentions";
import { postRegis } from "../../service/userService";
import { useNavigate } from "react-router-dom";
export default function RegisPage() {
  const navigate = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    postRegis(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành công");
        setTimeout(() => {
          navigate("/login");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.success("Đăng ký thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="84">+84</Option>
        <Option value="87">+87</Option>
      </Select>
    </Form.Item>
  );
  return (
    <div className="pt-10 bg-gradient-to-r from-orange-300 to-yellow-200 h-screen px-10 lg:px-60 md:px-20">
      <Form
        layout="vertical"
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 24,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Tài Khoản"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your Tài Khoản!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your Mật Khẩu!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your email!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Số Điện Thoại"
          name="soDt"
          rules={[
            {
              required: true,
              message: "Please input your Số Điện Thoại!",
            },
          ]}
        >
          <Input
            addonBefore={prefixSelector}
            style={{
              width: "100%",
            }}
          />
        </Form.Item>
        <Form.Item
          label="Mã Nhóm"
          name="maNhom"
          rules={[
            {
              required: true,
              message: "Please input your Mã Nhóm!",
            },
          ]}
        >
          <TreeSelect
            treeData={[
              {
                title: "GP00",
                value: "GP00",
                children: [{ title: "Bamboo", value: "bamboo" }],
              },
            ]}
          />
        </Form.Item>
        <Form.Item
          label="Họ Tên"
          name="hoTen"
          rules={[
            {
              required: true,
              message: "Please input your Họ Tên!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 24,
          }}
        ></Form.Item>

        <Form.Item
          className=" flex justify-center items-center"
          wrapperCol={{
            offset: 8,
            span: 24,
          }}
        >
          <Button className="bg-blue-400" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
