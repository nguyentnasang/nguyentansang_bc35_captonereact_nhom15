import React from "react";

export default function Snew() {
  return (
    <div className="pt-24">
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/joker.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            Tranh luận chi tiết Joker mang thai trong truyện tranh
          </b>
          <p className="my-5">18:54 12/1/2023 </p>
          <p className="truncate">
            Hàng loạt khán giả lên tiếng chỉ trích DC vì cho rằng câu chuyện
            Joker có thai đã "hủy hoại" tâm trí độc giả.
          </p>
        </div>
      </div>
      {/* ................  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/ant3.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            'Ant-man 3' là sự kiện trọng đại cho tương lai Marvel
          </b>
          <p className="my-5">09:26 12/1/2023 </p>
          <p className="truncate">
            Phó chủ tịch Marvel Studios - Stephen Broussard cho rằng tác động
            của “Ant-Man and the Wasp: Quantumania” đối với MCU có thể sánh
            ngang với sự kiện Civil War.
          </p>
        </div>
      </div>
      {/* ...............  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/merom.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            'Mẹ rơm' - kết thúc có hậu cho người đàn ông xấu xí nhất làng
          </b>
          <p className="my-5">06:48 12/1/2023 </p>
          <p className="truncate">
            Sau 22 năm, Mô "gù" nuôi Hạt Dẻ khôn lớn và anh hạnh phúc chứng kiến
            khoảnh khắc con gái nhận bằng tốt nghiệp đại học.
          </p>
        </div>
      </div>
      {/* .............  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/avatartt.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            Avatar và tham vọng chấm dứt kỷ nguyên phát trực tuyến
          </b>
          <p className="my-5">06:38 12/1/2023 </p>
          <p className="truncate">
            Đứa con tinh thần của James Cameron đã vực dậy thói quen ra rạp của
            một bộ phận đông đảo khán giả. Đây được kỳ vọng là dấu chấm hết cho
            "kỷ nguyên Streaming" nổi lên sau đại dịch.
          </p>
        </div>
      </div>
      {/* ..............  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/hocduong.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            Diễn viên nổi tiếng nhờ cảnh bạo lực học đường
          </b>
          <p className="my-5">21:18 11/1/2023 </p>
          <p className="truncate">
            Diễn xuất của Jung Ji So khi thể hiện hình ảnh nạn nhân bạo lực học
            đường được khán giả khen ngợi. Vai diễn giúp cô thu hút sự chú ý.
          </p>
        </div>
      </div>
      {/* ..............  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/thanos.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            Kẻ chinh phạt đa vũ trụ còn nguy hiểm hơn Thanos
          </b>
          <p className="my-5">18:10 11/1/2023 </p>
          <p className="truncate">
            Trailer "Ant-Man & the Wasp: Quantumania" vừa giới thiệu tới khán
            giả phản diện hoàn toàn mới: Kang - The Conqueror. Đây được cho là
            chìa khóa trong giai đoạn đa vũ trụ của Marvel.
          </p>
        </div>
      </div>
      {/* ............  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/2ty.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            'Avatar 2' sẽ kiếm được tối thiểu 2,5 tỷ USD
          </b>
          <p className="my-5">10:42 10/1/2023 </p>
          <p className="truncate">
            Bộ phim khoa học viễn tưởng của James Cameron liên tiếp chinh phục
            các cột mốc phòng vé. Phim dự kiến gặt hái 2,5-2,6 tỷ USD sau đợt
            công chiếu đầu tiên.
          </p>
        </div>
      </div>
      {/* .................  */}
      <div className="block ml-5 w-full justify-center items-center my-5 lg:flex md:flex">
        <img src={require("../../img/bupbe.jpg")} alt="" />
        <div className=" text-left ml-5 lg:w-1/2 md:w-1/2">
          <b className="truncate">
            Cuộc đụng độ nảy lửa giữa hai búp bê sát thủ
          </b>
          <p className="my-5">07:06 10/1/2023 </p>
          <p className="truncate">
            Ngay khi vừa ra mắt, M3GAN đã để lại nhiều dấu ấn và được người hâm
            mộ so sánh với biểu tượng kinh dị Chucky của "Child's Play".
          </p>
        </div>
      </div>
    </div>
  );
}

/* 

      
      
*/
