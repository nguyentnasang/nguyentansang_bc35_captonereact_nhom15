import React from "react";

export default function NotFoundPage() {
  return (
    <div className="h-screen w-screen bg-black  flex justify-center items-center text-4xl text-amber-300 ">
      <p className="animate-pulse">
        {" "}
        404 <br /> NOT FOUND PAGE
      </p>
    </div>
  );
}
