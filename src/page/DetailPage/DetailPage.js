import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getMovieItem, getMovieSchedule } from '../../service/movieService';
import { StarOutlined } from '@ant-design/icons';
import { Button, message } from 'antd';
import ReactPlayer from 'react-player/youtube';
import { AiOutlineHeart, AiOutlineShareAlt } from 'react-icons/ai';
import { CiSaveUp2 } from 'react-icons/ci';
import MovieTab from './MovieTab';
import moment from 'moment';
import 'moment/locale/vi';

export default function DetailPage() {
  const [movie, setMovie] = useState([]);
  const [movieSchedule, setMovieSchedule] = useState([]);
  console.log('movie', movie.maPhim);
  const params = useParams();
  console.log(params);
  useEffect(() => {
    getMovieItem(params.id)
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    getMovieSchedule(params.id)
      .then((res) => {
        console.log(res.data.content.heThongRapChieu);
        setMovieSchedule(res.data.content.heThongRapChieu);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(movieSchedule);
  return (
    <div className="bg-[#191b22]  font-libre">
      <div className="w-4/5 mx-auto">
        <div className="block grid-cols-4 pt-40  lg:grid md:grid">
          <img
            className="w-full rounded shadow-lg shadow-cyan-500/50 lg:col-span-1 md:col-span-2"
            src={movie.hinhAnh}
            alt=""
          />
          <div className="text-left ml-32 text-gray-100 flex flex-col items-start lg:col-span-3 md:col-span-2">
            <span className="my-5 text-4xl tracking-normal font-normal text-gray-100">
              {movie.tenPhim}
            </span>
            <span className="mt-5 text-gray-300">
              <b>Mã Phim:</b> {movie.maPhim}
            </span>
            <span className="mt-5 text-gray-300">
              <b>Ngày Khởi Chiếu: </b>
              {moment(movie.ngayKhoiChieu).format('L')}
            </span>
            <span className="flex items-center mt-5 text-gray-300">
              <b> Đánh giá: </b> {movie.danhGia}
              <StarOutlined className="text-yellow-500" />
            </span>
            <span className="mt-5 text-gray-300">
              <b>Giá vé: </b> 57.000đ
            </span>
            <div className="flex  text-[#C8235D] space-x-5 mt-5 items-center">
              <span className="cursor-pointer text-3xl">
                <AiOutlineHeart />
              </span>
              <span className="cursor-pointer text-3xl">
                <AiOutlineShareAlt />
              </span>
              <span className="cursor-pointer text-3xl">
                <CiSaveUp2 />
              </span>
              <button className="text-gray-300 hover:bg-[#C8235D] duration-300 text-md border border-gray-400 rounded-lg px-5 py-2">
                Xem Trailer
              </button>
            </div>
            <a
              onClick={() => {
                message.success('Đặt vé thành công');
              }}
              href="#_"
              class="mt-8 relative inline-flex items-center justify-start px-8 py-3 overflow-hidden font-medium transition-all bg-[#7F66DE] rounded-xl group"
            >
              <span class="absolute top-0 right-0 inline-block w-4 h-4 transition-all duration-500 ease-in-out bg-[#8e74f7] rounded group-hover:-mr-4 group-hover:-mt-4">
                <span class="absolute top-0 right-0 w-5 h-5 rotate-45 translate-x-1/2 -translate-y-1/2 bg-white"></span>
              </span>
              <span class="absolute bottom-0 left-0 w-full h-full transition-all duration-500 ease-in-out delay-200 -translate-x-full translate-y-full bg-[#8e74f7]   rounded-2xl group-hover:mb-12 group-hover:translate-x-0"></span>
              <span class="relative w-full text-left text-white transition-colors duration-200 ease-in-out group-hover:text-white">
                Đặt Vé Ngay
              </span>
            </a>
          </div>
        </div>
        <div className="text-white text-left mt-8">
          <h2 className="font-normal text-2xl tracking-wide">Description</h2>
          <p className="text-gray-400 mt-2 text-left tracking-wide">
            {movie.moTa}
          </p>
        </div>
      </div>

      <div className="flex flex-col justify-center items-center w-full mt-5 my-32">
        <p className="font-medium text-3xl animate-pulse text-white py-5 mb-2">
          TRAILER
        </p>
        <ReactPlayer
          width={'1000px'}
          height={'500px'}
          className="scale-105"
          controls="true"
          url={movie.trailer}
        />
      </div>
      <h2 className="text-2xl font-libre font-semibold tracking-wide text-center text-white mt-8 ">
        Hiện đang chiếu tại:
      </h2>
      {movieSchedule && <MovieTab schedule={movieSchedule} />}
    </div>
  );
}
