import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import moment from 'moment';
import 'moment/locale/vi';
import { NavLink } from 'react-router-dom';
moment.locale('vi');

export default function MovieTab({ schedule }) {
  console.log(schedule);
  //   let [schedule, setschedule] = useState([]);

  //   useEffect(() => {
  //     getMovieschedule()
  //       .then((res) => {
  //         // console.log(res);
  //         setschedule(res.data.content);
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //       });
  //   }, []);
  let days = [
    'Thứ hai',
    'Thứ ba',
    'Thứ tư',
    'Thứ năm',
    'Thứ sáu',
    'Thứ bảy',
    'Chủ nhật',
  ];
  const onChange = (key) => {
    console.log(key);
  };

  const renderHeThongRap = () => {
    return schedule.map((item) => {
      return {
        label: (
          <>
            <img className="w-16" src={item.logo} alt="" />
            <p className="font-libre font-semibold">{item.maHeThongRap}</p>
          </>
        ),
        key: item.tenHeThongRap,
        children: (
          <Tabs
            className="h-96 w-full flex"
            tabPosition="top"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumrapTheoHeThongRap(item)}
          />
        ),
      };
    });
  };
  const renderDays = (item) => {
    console.log(item.lichChieuPhim);
    return days.map((day) => {
      return {
        label: <p className="text-left flex">{day}</p>,
        key: day,
        children: (
          // <div className="h-96 text-left overflow-scroll">
          //   {days.map((day) => {
          //     return <p>{day}</p>;
          //   })}
          // </div>
          <div className="text-left text-xl text-green-500 font-semibold grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-5 pr-3">
            {item.lichChieuPhim.map((lichChieu) => {
              if (
                moment(lichChieu.ngayChieuGioChieu).format('dddd') ==
                day.toLowerCase()
              )
                return (
                  <NavLink
                    to={`/chitietphhongve/${lichChieu.maLichChieu}`}
                    className="w-1/4"
                  >
                    {moment(lichChieu.ngayChieuGioChieu).format('LT')}
                  </NavLink>
                );
            })}
          </div>
        ),
      };
    });
  };
  const renderCumrapTheoHeThongRap = (item) => {
    // console.log("itemaaaa", item);
    return item.cumRapChieu.map((item) => {
      console.log(item.lichChieuPhim);
      return {
        label: (
          <div className="text-left text-white flex">
            <div>
              <img className="w-20 rounded mr-4" src={item.hinhAnh} alt="" />
            </div>
            <div>
              <p className="truncate">{item.tenCumRap}</p>
              <p className="truncate">{item.diaChi}</p>
            </div>
          </div>
        ),
        key: item.maCumRap,
        children: (
          // <div className="h-96 text-left overflow-scroll">
          //   {days.map((day) => {
          //     return <p>{day}</p>;
          //   })}
          // </div>
          <Tabs
            className="h-96 text-white"
            tabPosition="top"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderDays(item)}
          />
        ),
      };
    });
  };
  // const renderLichChieuPhimTheoCumRap = (item) => {
  //   // console.log("item", item);
  //   return item.lichChieuPhim.map((item) => {
  //     return (
  //       <div className="flex">
  //         <img className="w-20 h-30 mt-5" src={item.hinhAnh} alt="" />
  //         <div className="ml-3">
  //           <p className="text-left mt-5 font-medium">{item.tenPhim}</p>
  //           <div className="grid grid-cols-2 gap-5">
  //             {renderLichChieuTheoPhim(item)}
  //           </div>
  //         </div>
  //       </div>
  //     );
  //   });
  // };
  const renderLichChieuTheoPhim = (item) => {
    return item.lichChieuPhim.map((lichChieu) => {
      console.log(lichChieu.ngayChieuGioChieu);
      return <p>{moment(lichChieu.ngayChieuGioChieu).format('dddd')}</p>;
    });
  };
  return (
    <div className="flex justify-center items-center pt-5 pb-16">
      <Tabs
        className="h-96 bg-gradient-to-b from-color1 to-color5 rounded w-3/4 text-white"
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
// {
//     label: `Tab 1`,
//     key: "1",
//     children: `Content of Tab Pane 1`,
//   },
