import React from 'react';
import { Button, Form, Input, message } from 'antd';
import { postLogin } from './../../service/userService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUserInfo } from '../../redux-toolkit/userSlice';
import { userLocalService } from '../../service/localService';
import Lottie from 'lottie-react';
import groovyWalkAnimation from '../../assets/31162-movie-engagement.json';
import '../../Mycss.css';
export default function LoginPage() {
  let dispath = useDispatch();
  const navigate = useNavigate();
  const onFinish = (values) => {
    // console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        console.log(res);
        userLocalService.set(res.data.content);
        message.success('Đăng nhập thành công');
        dispath(setUserInfo(res.data.content));
        setTimeout(() => {
          navigate('/');
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error('Đăng nhập thất bại');
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="pl-10 pt-10 text-left block justify-center items-center w-screen  bg-gradient-to-r from-orange-300 to-yellow-200 h-screen lg:flex md:flex">
      <div className="w-1/2">
        <div className="w-3/4 hidden lg:block md:block">
          <Lottie animationData={groovyWalkAnimation} loop={true} />;
        </div>
      </div>

      <div id="tv" className="w-full h-full flex flex-col justify-center">
        <div className="h-full w-full flex flex-col justify-center items-center ">
          <Form
            className="w-full p-5 flex flex-col justify-center font-medium "
            layout="vertical"
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <span>Tài Khoản:</span>
            <Form.Item
              name="taiKhoan"
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input />
            </Form.Item>
            <span>Mật Khẩu:</span>
            <Form.Item
              name="matKhau"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              // wrapperCol={{ offset: 8, span: 24 }}
            ></Form.Item>

            <Form.Item
              className="flex flex-col justify-center items-center llllll"
              // wrapperCol={{ offset: 8, span: 24 }}
            >
              <Button
                className="flex flex-col justify-center items-center w-full px-10 py-4"
                htmlType="submit"
              >
                Đăng nhập
              </Button>
              <Button
                className="flex flex-col justify-center items-center w-full px-10 py-4 mt-5"
                onClick={() => {
                  window.location.href = '/regis';
                }}
                htmlType="submit"
              >
                Đăng Ký
              </Button>
            </Form.Item>
            <p>
              nếu bạn là Admin hãy đăng nhập{' '}
              <button
                className="text-yellow-900 font-medium"
                onClick={() => {
                  window.location.href = '/admin/login';
                }}
              >
                Ở Đây
              </button>
            </p>
          </Form>
        </div>
      </div>
    </div>
  );
}
