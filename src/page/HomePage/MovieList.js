import React, { useEffect, useState } from "react";
import { Card, message } from "antd";
import Meta from "antd/es/card/Meta";
import { getMovie } from "./../../service/movieService";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

export default function MovieList() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const [movieArr, setMovieArr] = useState([]);
  //   console.log("movieArr", movieArr);
  useEffect(() => {
    getMovie()
      .then((res) => {
        // console.log("sang", res);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderMovieList = () => {
    return movieArr.slice(0, 20).map((movie) => {
      return (
        <Card
          className="group hover:border-white bg-gradient-to-t from-[#292929] to-[#505050] border-none shadow-2xl shadow-slate-800"
          key={movie.maPhim}
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img
              className="group-hover:brightness-50 transition-all h-96 object-cover"
              alt="example"
              src={movie.hinhAnh}
            />
          }
        >
          <Meta
            title={
              <h2 className="text-2xl font-semi tracking-tight text-yellow-50 dark:text-white h-10 truncate">
                {movie.tenPhim}
              </h2>
            }
            description={
              <p className="text-left text-gray-400">
                {movie.moTa < 60 ? movie.moTa : movie.moTa.slice(0, 60) + "..."}
              </p>
            }
          />
          <div className="mt-7">
            <button
              onClick={() => {
                if (user) {
                  window.location.href = `/detail/${movie.maPhim}`;
                } else {
                  message.error("bạn chưa đăng nhập");
                }
              }}
              className="flex items-center text-sm font-medium text-center text-white bg-blue-00 justify-center  hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-[#C8235D] dark:hover:bg-blue-700 dark:focus:ring-blue-800 px-5 py-2 rounded mx-0"
            >
              Chi Tiết
              <svg
                aria-hidden="true"
                className="w-4 h-4 ml-2 -mr-1 align-text-bottom"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
          </div>
        </Card>
      );
    });
  };
  return (
    <div className="py-10 px-20 lg:px-30 md:px-30">
      <p id="h123" className="text-white text-left  font-medium text-3xl mb-5">
        ĐANG CHIẾU
      </p>
      <div className="grid grid-cols-1 gap-5 lg:grid-cols-5 md:grid-cols-3 ">
        {renderMovieList()}
      </div>
    </div>
  );
}
