import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { getMovieTheater } from './../../service/movieService';
import moment from 'moment';
import 'moment/locale/vi';
moment.locale('vi');
export default function MovieTab() {
  let [theater, setTheater] = useState([]);

  useEffect(() => {
    getMovieTheater()
      .then((res) => {
        console.log(res.data.content);
        setTheater(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };
  const renderHeThongRap = () => {
    return theater.map((item) => {
      return {
        label: <img className="w-16" src={item.logo} alt="" />,
        key: item.tenHeThongRap,
        children: (
          <Tabs
            className="h-96"
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumrapTheoHeThongRap(item)}
          />
        ),
      };
    });
  };
  const renderCumrapTheoHeThongRap = (item) => {
    // console.log("itemaaaa", item);
    return item.lstCumRap.map((item) => {
      return {
        label: (
          <div className="text-left w-48">
            <p className="truncate">{item.tenCumRap}</p>
            <p className="truncate">{item.diaChi}</p>
          </div>
        ),
        key: item.maCumRap,
        children: (
          <div className="h-96 overflow-scroll">
            {renderDanhSachPhimTheoCumRap(item)}
          </div>
        ),
      };
    });
  };
  const renderDanhSachPhimTheoCumRap = (item) => {
    // console.log("item", item);
    return item.danhSachPhim.map((item) => {
      return (
        <div className="flex">
          <img className="w-20 h-30 mt-5" src={item.hinhAnh} alt="" />
          <div className="ml-3">
            <p className="text-left mt-5 font-medium">{item.tenPhim}</p>
            <div className="grid grid-cols-2 gap-5">
              {renderLichChieuTheoPhim(item)}
            </div>
          </div>
        </div>
      );
    });
  };
  const renderLichChieuTheoPhim = (item) => {
    return item.lstLichChieuTheoPhim.slice(0, 4).map((item) => {
      return <p>{moment(item.ngayChieuGioChieu).format('LLL')}</p>;
    });
  };
  return (
    <div className="flex justify-center items-center py-10">
      <Tabs
        className="h-96 bg-white rounded"
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
// {
//     label: `Tab 1`,
//     key: "1",
//     children: `Content of Tab Pane 1`,
//   },
