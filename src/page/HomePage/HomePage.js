import React from 'react';
import Carousels from './Carousels';
import MovieList from './MovieList';
import MovieTab from './MovieTab';
import ReactPlayer from 'react-player/youtube';
import Background from '../../img/backgroud.jpg';
import Tabbar from './Tabbar';

export default function HomePage() {
  return (
    <div
      style={{ backgroundImage: 'url(' + Background + ')' }}
      className="bg-gradient-to-r from-slate-900 to-gray-700 font-libre"
    >
      <Carousels />

      <MovieList />

      <p className="text-yellow-50 font-medium text-3xl animate-pulse mt-5 hidden md:block lg:block">
        HỆ THỐNG RẠP
      </p>
      <div className="hidden md:block lg:block">
        <MovieTab />
      </div>
      <Tabbar />
    </div>
  );
}
