import React from "react";
import { Button, Tabs } from "antd";
export default function Tabbar() {
  const onChange = (key) => {
    console.log(key);
  };
  const items = [
    {
      key: "1",
      label: <b className="text-3xl">Tin Tức 24h</b>,
      children: (
        <Button
          onClick={() => {
            window.location.href = "/snew";
          }}
          className="bg-blue-300 text-yellow-100 font-medium my-16"
        >
          Xem Thêm
        </Button>
      ),
    },
    {
      key: "2",
      label: <b className="text-3xl">APP</b>,
      children: (
        <Button
          onClick={() => {
            window.location.href = "/app";
          }}
          className="bg-blue-300 text-yellow-100 font-medium my-16"
        >
          Xem Thêm
        </Button>
      ),
    },
    {
      key: "3",
      label: <b className="text-3xl">Khuyến Mãi</b>,
      children: (
        <Button
          onClick={() => {
            window.location.href = "/promotion";
          }}
          className="bg-blue-300 text-yellow-100 font-medium my-16"
        >
          Xem Thêm
        </Button>
      ),
    },
  ];
  return (
    <div className="flex justify-center items-center">
      <Tabs
        className="text-white"
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
      />
      ;
    </div>
  );
}
