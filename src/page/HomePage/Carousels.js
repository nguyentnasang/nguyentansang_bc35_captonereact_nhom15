import React from "react";
import { Carousel } from "antd";
import "../../Mycss.css";
import banner1 from "../../img/banner1.jpg";
import banner2 from "../../img/banner2.jpg";
import banner3 from "../../img/banner3.jpg";
import banner4 from "../../img/banner4.jpg";
import banner5 from "../../img/shangchi.jpg";
import { PlayCircleOutlined } from "@ant-design/icons";
const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "black",
};
export default function Carousels() {
  return (
    <div className="relative h-full">
      {/* autoplay */}
      <Carousel>
        <div className="bg-slate-900 z-10 relative">
          <img
            className="h-fullbrightness-80 h-[694px] w-full object-cover"
            src={banner5}
            alt=""
          />
          <button
            onClick={() => {
              window.location.href =
                "https://www.youtube.com/watch?v=FwtK6FG_Hvg";
            }}
            className="absolute top-1/2 left-1/2"
          >
            <PlayCircleOutlined style={{ fontSize: "50px", color: "white" }} />
          </button>
        </div>
        <div className="bg-slate-900 z-10 relative">
          <img
            className="brightness-80 h-[694px] w-full object-cover"
            src={banner2}
            alt=""
          />
          <button
            onClick={() => {
              window.location.href =
                "https://www.youtube.com/watch?v=g8_DQqqTabk";
            }}
            className="absolute top-1/2 left-1/2"
          >
            <PlayCircleOutlined style={{ fontSize: "50px", color: "white" }} />
          </button>
        </div>
        <div className="bg-slate-900 z-10 relative">
          <img
            className="brightness-80 h-[694px] w-full object-cover"
            src={banner3}
            alt=""
          />
          <button
            onClick={() => {
              window.location.href =
                "https://www.youtube.com/watch?v=DzHqxgcY5Io";
            }}
            className="absolute top-1/2 left-1/2"
          >
            <PlayCircleOutlined style={{ fontSize: "50px", color: "white" }} />
          </button>
        </div>
        <div className="bg-slate-900 z-10 relative">
          <img
            className="brightness-80 h-[694px] w-full object-cover"
            src={banner4}
            alt=""
          />
          <button
            onClick={() => {
              window.location.href =
                "https://www.youtube.com/watch?v=d9MyW72ELq0";
            }}
            className="absolute top-1/2 left-1/2"
          >
            <PlayCircleOutlined style={{ fontSize: "50px", color: "white" }} />
          </button>
        </div>
      </Carousel>
      <br />
    </div>
  );
}
