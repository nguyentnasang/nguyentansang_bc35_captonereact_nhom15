import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './page/HomePage/HomePage';
import LoginPage from './page/LoginPage/LoginPage';
import NotFoundPage from './page/NotFoundPage/NotFoundPage';
import DetailPage from './page/DetailPage/DetailPage';
import Layout from './HOC/Layout';
import RegisPage from './page/RegisPage/RegisPage';
import AdminLogin from './Admin_Page/AdminLogin';
import ListLoginPage from './Admin_Page/ListLoginPage';
import Spinner from './component/Spinner/Spinner';
import Snew from './page/Snew/Snew';
import Promotion from './page/Promotion/Promotion';
import Apppage from './page/Apppage.js/Apppage';
import TicketPage from './page/TicketPage/TicketPage';
function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <DetailPage />
              </Layout>
            }
          />
          <Route
            path="/snew"
            element={
              <Layout>
                <Snew />
              </Layout>
            }
          />
          <Route
            path="/promotion"
            element={
              <Layout>
                <Promotion />
              </Layout>
            }
          />
          <Route
            path="/app"
            element={
              <Layout>
                <Apppage />
              </Layout>
            }
          />
          <Route
            path="/chitietphhongve/:maLichChieu"
            element={
              <Layout>
                <TicketPage />
              </Layout>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="*" element={<NotFoundPage />} />
          <Route path="/regis" element={<RegisPage />} />
          <Route path="/admin/login" element={<AdminLogin />} />
          <Route path="/admin/listlogin" element={<ListLoginPage />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
