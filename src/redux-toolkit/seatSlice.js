import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  dangChonList: [], // model nay de post len api, khong co tenGhe kem theo
  selectedList: [], // cai nay de render, co tenGhe kem theo de render
};

const seatSlice = createSlice({
  name: "seatSlice",
  initialState,
  reducers: {
    setSelectedList: (state, { type, payload }) => {
      let index = state.selectedList.findIndex(
        (item) => item.maGhe === payload.maGhe
      );
      if (index === -1) {
        state.dangChonList.push({
          maGhe: payload.maGhe,
          giaVe: payload.giaVe,
        });
        state.selectedList.push({
          maGhe: payload.maGhe,
          giaVe: payload.giaVe,
          tenGhe: payload.tenGhe,
        });
      } else
        state.dangChonList.splice(index, 1) &&
          state.selectedList.splice(index, 1);
    },
  },
});

export const { setSelectedList } = seatSlice.actions;

export default seatSlice.reducer;
