import { createSlice } from "@reduxjs/toolkit";
import { userLocalService } from "../service/localService";

const initialState = {
  user: userLocalService.get(),
  userArr: "",
  findUser: "",
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfo: (state, action) => {
      state.user = action.payload;
    },
    setUserArrInfo: (state, action) => {
      state.userArr = action.payload;
    },
    setUserFindInFo: (state, action) => {
      state.findUser = action.payload;
    },
  },
});

export const { setUserInfo, setUserArrInfo, setUserFindInFo } =
  userSlice.actions;

export default userSlice.reducer;
