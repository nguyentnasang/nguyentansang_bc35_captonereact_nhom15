import React from "react";
import { ClimbingBoxLoader } from "react-spinners";
import { useSelector } from "react-redux";
export default function Spinner() {
  let isLoading = useSelector((state) => {
    return state.spinnerSlice.isLoading;
  });
  return isLoading ? (
    <div className="fixed z-50 h-screen w-screen top-0 left-0 bg-black flex justify-center items-center">
      <ClimbingBoxLoader size={25} speedMultiplier={2} color="yellow" />
    </div>
  ) : (
    <></>
  );
}
