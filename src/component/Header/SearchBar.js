import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { FiSearch } from 'react-icons/fi';

export default function Searchbar() {
  const navigate = useNavigate();
  const [searchTerm, setSearchTerm] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    navigate(`/search/${searchTerm}`);
  };
  return (
    <form
      onSubmit={handleSubmit}
      autoComplete="off"
      className="text-gray-400 group  focus-within:text-gray-600 border-none lg:border border-gray-600 rounded-xl"
    >
      <label htmlFor="search-field" className="sr-only">
        Search films
      </label>
      <div className="flex items-center">
        <FiSearch className="ml-4 max-w-5 text-white" />
        <input
          autoComplete="off"
          name="search-field"
          id="search-field"
          value={searchTerm}
          onChange={(e) => {
            setSearchTerm(e.target.value);
          }}
          placeholder="Tra cứu phim"
          className="focus:outline-none focus:border placeholder:text-gray-400 border-none bg-transparent lg:p-3 p-2 text-base text-white placeholder-gray-500 outline-none "
        />
      </div>
    </form>
  );
}
