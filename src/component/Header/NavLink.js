import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";

export default function NavLink() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const renderNavLink = () => {
    if (user) {
      return (
        <div className="flex">
          <div className="text-white flex items-center"> {user?.hoTen} </div>
          <button
            onClick={() => {
              userLocalService.remove();
              window.location.reload();
            }}
            className="ml-3 bg-[#C8235D] px-3 py-2 rounded hover:brightness-75 transition-all"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex">
          <button
            onClick={() => {
              window.location = "/login";
            }}
            className="ml-3 bg-[#3D3D3D] px-3 py-2 rounded hover:brightness-75 transition-all"
          >
            Đăng Nhập
          </button>
          <button
            onClick={() => {
              window.location = "/regis";
            }}
            className="ml-3 bg-[#7F66DE] px-3 py-2 rounded hover:brightness-75 transition-all"
          >
            Đăng Ký
          </button>
        </div>
      );
    }
  };
  // console.log("user", user);
  return (
    <div className="flex flex-col justify-center items-center">
      {renderNavLink()}
    </div>
  );
}
