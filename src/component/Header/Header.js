import React from 'react';
import NavLink from './NavLink';
import BackToTop from 'react-back-to-top-button';
import { UpOutlined } from '@ant-design/icons';
import Searchbar from './SearchBar';
import { FaPlay } from 'react-icons/fa';
import { AiOutlineClose } from 'react-icons/ai';

import { useState } from 'react';
export default function Header() {
  const [mobileOpen, setMobileOpen] = useState(false);
  const handleMobileOpen = () => {
    mobileOpen == false ? setMobileOpen(true) : setMobileOpen(false);
  };
  return (
    <nav className="flex bg-black justify-between items-center py-5 px-5 lg:px-10 xl:px-40 absolute z-10 w-full bg-opacity-30 text-yellow-100 shadow-lg">
      <BackToTop
        // showOnScrollUp
        showAt={100}
        speed={1500}
        easing="easeInOutQuint"
      >
        <span className="text-white animate-ping">
          <UpOutlined />
        </span>
      </BackToTop>

      <span className="font-bold text-3xl animate-bounce mr-4">
        <a className="text-[#c8235d] trackign " href="/">
          SMOVIE
          <div className="text-white flex -mt-2">
            <div className="text-xl items-center flex mr-1">
              <FaPlay />
            </div>
            cinema
          </div>
        </a>
      </span>
      <button
        onClick={handleMobileOpen}
        className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
      >
        <span className="sr-only">Open main menu</span>
        <svg
          className="w-6 h-6"
          aria-hidden="true"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
            clipRule="evenodd"
          />
        </svg>
      </button>
      <div className="lg:flex items-center hidden w-full justify-between">
        <ul className="gap-x-7 hidden md:flex lg:flex">
          <li>
            <a
              className="text-white lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
              href="/"
            >
              Trang chủ
            </a>
          </li>
          <li>
            <a
              className="text-white flex lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
              href="/snew"
            >
              Tin Tức
            </a>
          </li>
          <li>
            <a
              className="text-white lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
              href="/promotion"
            >
              Khuyến mãi
            </a>
          </li>
          <li>
            <a
              className="text-white lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
              href="/"
            >
              Phim Mới
            </a>
          </li>
        </ul>
        <Searchbar />
        <NavLink />
      </div>
      {mobileOpen && (
        <div className="bg-black/80 absolute inset-0 w-full h-screen lg:hidden ">
          <AiOutlineClose
            onClick={handleMobileOpen}
            className="top-0 right-0 absolute text-3xl z-10 cursor-pointer"
          />
          <div className="flex flex-col w-screen h-screen items-center lg:hidden justify-center space-y-20">
            <ul className="gap-7 flex flex-col">
              <li className="flex justify-center">
                <a
                  className="text-white lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
                  href="/"
                >
                  Trang chủ
                </a>
              </li>
              <li className="flex justify-center">
                <a
                  className="text-white flex lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
                  href="/snew"
                >
                  Tin Tức
                </a>
              </li>
              <li className="flex justify-center">
                <a
                  className="text-white lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
                  href="/promotion"
                >
                  Khuyến mãi
                </a>
              </li>
              <li className="flex justify-center">
                <a
                  className="text-white lg:text-lg text-sm font-normal hover:text-[#C8235D] transition-all whitespace-nowrap"
                  href="/"
                >
                  Phim Mới
                </a>
              </li>
            </ul>
            <Searchbar />
            <NavLink />
          </div>
        </div>
      )}
    </nav>
  );
}
