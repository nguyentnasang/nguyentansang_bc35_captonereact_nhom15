/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      gridTemplateColumns: {
        // Simple 16 column grid
        16: 'repeat(16, minmax(0, 1fr))',
      },
      gridTemplateRows: {
        // Simple 8 row grid
        10: 'repeat(10, minmax(0, 1fr))',
      },
      fontFamily: {
        libre: ['Montserrat', 'sans-serif'],
      },
      colors: {
        color1: '#3D3D3D',
        color2: '#C8235D',
        color3: '#7F66DE',
        color4: '#C4C4C4',
        color5: '#101116',
      },
    },
  },
  plugins: [],
};
